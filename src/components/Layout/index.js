import Design from "../connectXD";
import Header from "../Navbar";

import Register from "../Register";


const Layout = () => {
    return (
      <div>
        <div className="bg-blue container-fluid">
          {/* navbar */}
          <div>
            <Header />
          </div>
          {/* section-1 */}
          <div>
            <Design />
          </div>{" "}
        </div>
        {/* section-2 */}
        <div>
          <Register />
        </div>
      </div>
    );
}
export default Layout;