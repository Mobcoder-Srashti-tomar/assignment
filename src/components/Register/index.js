import RegisterImg from "../../assets/images/learning-exp.png";
import Card from "../../assets/images/Icon.png";

const Register = () => {
    return (
      <>
        <div>
          <div className="row common-padding   align-items-center main">
            <div className="col-md-6">
              <img src={RegisterImg} alt="register-img"></img>
            </div>
            <div className="col-md-4">
              <p className="learn-para">Learn Anywhere, Anytime</p>
              <h4 className="positive-para">
                Positive Learning Experiences <br></br> At Your Fingertips
              </h4>
              <p className="content">
                Access digital educational content directly on your mobile
                device and interact with a learning bot through any one of your
                preferred social messaging platforms. Come collaborate with
                peers and educators from around the world, and exchange
                knowledge and ideas as life-long learners.
              </p>
              <div>
                <button type="button" class="btn btn-success">
                  REGISTER AS LEARNER
                </button>
              </div>
            </div>
          </div>
        </div>

        <div className="container mt-5 text-center">
          <h2 className="positive-para text-align-center">
            Empowering Minds, Impacting Communities
          </h2>
          <p className="content text-center para-width">
            We’re becoming the premiere eLearning hub for remote workers and
            communities across the globe, offering localized content that
            reflects their various cultures, languages, and learning styles.
          </p>
        </div>
        <div className="card-box">
          <div className="row">
            <div className="col-md-3">
              <div className="card">
                <img src={Card}></img>
                <h2 className="number">742+</h2>
                <h6 className="line">Active Learners</h6>
              </div>
            </div>
            <div className="col-md-3">
              <div className="card">
                <img src={Card}></img>
                <h2 className="number">65+</h2>
                <h6 className="line">Educators</h6>
              </div>
            </div>
            <div className="col-md-3">
              <div className="card">
                <img src={Card}></img>
                <h2 className="number">X</h2>
                <h6 className="line">Courses Available</h6>
              </div>
            </div>
            <div className="col-md-3">
              <div className="card">
                <img src={Card}></img>
                <h2 className="number">X</h2>
                <h6 className="line">Communities Reached</h6>
              </div>
            </div>
          </div>
        </div>
      </>
    );
};
export default Register;
