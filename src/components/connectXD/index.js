
import MainImg from "../../assets/images/banner-img.png";
import Mini from "../../assets/images/logo-mini.png";

const Design = () => {
    return (
      <div>
        <div className="row justify-content-evenly mt-5 align-items-center main-box">
                <div className="col-md-4">
                    <img src = {Mini} alt = "mini"></img>
            <h4 className="text-white build-text">
              Let's build skills with IFA & learn without limits ...{" "}
            </h4>
            <p className="main-para">Take your learning to the next level</p>
          </div>
          <div className="col-md-6">
            <img src={MainImg}></img>
          </div>
            </div>
            
      </div>
    );
}
export default Design;