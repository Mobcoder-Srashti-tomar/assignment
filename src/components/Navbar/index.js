import NavImg from "../../assets/images/logo-white.png";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";


const Header = () => {
    return (
      <div className="header">
        <Navbar expand="lg">
          <Container fluid>
            <Navbar.Brand href="#">
              <img src={NavImg} alt="Logo" />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="navbarScroll" />
            <Navbar.Collapse id="navbarScroll">
              <Nav
                className="me-auto my-2 my-lg-0"
                style={{ maxHeight: "100px" }}
                navbarScroll
              >
                <Nav.Link href="#action1" className="link">
                  Home
                </Nav.Link>
                <Nav.Link href="#action2" className="link">
                  About Us
                </Nav.Link>

                <Nav.Link href="#" className="link">
                  Courses
                </Nav.Link>
                <Nav.Link href="#" className="link">
                  Contact
                </Nav.Link>
              </Nav>
              <Nav.Link href="#action2" className="link">
                Login
              </Nav.Link>
              <button type="button" class="btn btn-success">
                Get Started
              </button>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </div>
    );
};

export default Header;

